package spring_esvari.controllers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import spring_esvari.models.WordReview;
import spring_esvari.services.WordReviewServiceable;

import java.util.List;

@RestController
@RequestMapping("/api/wordsreview")
public class WordReviewController {
    private final WordReviewServiceable wordReviewServiceable;

    @Autowired
    public WordReviewController(WordReviewServiceable wordReviewServiceable) {
        this.wordReviewServiceable = wordReviewServiceable;
    }

    @GetMapping("/{id}")
    public List<WordReview> wordById() {
        return wordReviewServiceable.getAllWordReview();
    }

    @GetMapping
    public List<WordReview> searchWords(@RequestParam(value = "search", required =
            false) String search) {
        return wordReviewServiceable.searchWords(search);
    }
}