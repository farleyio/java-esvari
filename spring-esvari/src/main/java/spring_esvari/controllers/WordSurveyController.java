package spring_esvari.controllers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import spring_esvari.models.WordSurvey;
import spring_esvari.services.WordSurveyServiceable;

import java.util.List;

@RestController
@RequestMapping("/api/wordssurvey")
public class WordSurveyController {
    private final WordSurveyServiceable wordSurveyServiceable;

    @Autowired
    public WordSurveyController(WordSurveyServiceable wordSurveyServiceable) {
        this.wordSurveyServiceable = wordSurveyServiceable;
    }

    @GetMapping("/{id}")
    public List<WordSurvey> wordById() {
        return wordSurveyServiceable.getAllWordReview();
    }

    @GetMapping
    public List<WordSurvey> searchWords(@RequestParam(value = "search", required =
            false) String search) {
        return wordSurveyServiceable.searchWords(search);
    }
}
