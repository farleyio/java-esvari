package spring_esvari.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import spring_esvari.models.Word;
import spring_esvari.services.WordServiceable;

import java.util.ArrayList;
import java.util.UUID;
@RestController
@RequestMapping("/api/words")
public class WordController {
    private final WordServiceable wordServiceable;
    @Autowired
    public WordController(WordServiceable wordServiceable) {
        this.wordServiceable = wordServiceable;
    }

    @GetMapping("/{id}")
    public Word wordById(@PathVariable UUID id) {
        return wordServiceable.wordById(id);
    }

    @GetMapping
    public ArrayList<Word> searchWords(@RequestParam(value = "search", required = false) String search) {
        return wordServiceable.searchWords(search);
    }

    @PostMapping
    public void addWord(@RequestBody Word word) {
        wordServiceable.saveWord(word);
    }

    @PutMapping("/{id}")
    public void updateWord(@PathVariable UUID id, @RequestBody Word word) {
        Word existingWord = wordServiceable.wordById(id);
        if (existingWord != null) {
            word.setId(id);
            wordServiceable.updateWord(word);
        }
    }

}