package spring_esvari.services;

import org.springframework.data.jpa.repository.JpaRepository;
import spring_esvari.models.Word;

import java.util.ArrayList;
import java.util.UUID;

public interface WordRepository extends JpaRepository<Word, UUID> {
    ArrayList<Word> findByWordEnglishContainingIgnoreCaseOrWordSpanishContainingIgnoreCase(String wordEnglish, String wordSpanish);

}
