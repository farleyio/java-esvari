package spring_esvari.services;

import spring_esvari.models.Word;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public interface WordServiceable {

    Word wordById(UUID id);
    void saveWord(Word word);
    void updateWord(Word word);
    ArrayList<Word> searchWords(String search);

}
