package spring_esvari.services;

import org.springframework.data.jpa.repository.JpaRepository;
import spring_esvari.models.WordReview;
import spring_esvari.models.WordSurvey;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public interface WordSurveyRepository extends JpaRepository<WordSurvey, UUID> {
    List<WordSurvey> findByWordEnglishContainingIgnoreCaseOrWordSpanishContainingIgnoreCase(String wordEnglish,
                                                                                            String wordSpansih);
}

