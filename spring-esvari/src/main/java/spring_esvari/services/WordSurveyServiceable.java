package spring_esvari.services;
import spring_esvari.models.WordReview;
import spring_esvari.models.WordSurvey;

import java.util.List;

public interface WordSurveyServiceable {
    List<WordSurvey> getAllWordReview();

    List<WordSurvey> searchWords(String search);
}