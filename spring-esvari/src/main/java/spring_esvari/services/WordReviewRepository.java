package spring_esvari.services;

import org.springframework.data.jpa.repository.JpaRepository;
import spring_esvari.models.WordReview;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public interface WordReviewRepository extends JpaRepository<WordReview, UUID> {
    List<WordReview> findByWordEnglishContainingIgnoreCaseOrWordSpanishContainingIgnoreCase(String wordEnglish,
                                                String wordSpansih);
}
