package spring_esvari.services;
import spring_esvari.models.WordReview;

import java.util.List;

public interface WordReviewServiceable {
    List<WordReview> getAllWordReview();

    List<WordReview> searchWords(String search);


}
