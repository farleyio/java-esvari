package spring_esvari.models;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import java.util.UUID;

@Entity
@Table(name = "words")
@Getter
@Setter
@NoArgsConstructor // empty constructor for JPA
public class Word {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;

    @Column(nullable = false)
    private String wordEnglish;

    @Column(nullable = false)
    private String wordSpanish;

    @Column(length = 1024)
    private String description;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private Country country;

   // public Word(){}

    public Word(String wordEnglish, String wordSpanish, Country country, String description) {
        this.wordEnglish = wordEnglish;
        this.wordSpanish = wordSpanish;
        this.country = country;
        this.description = description;
    }
}
