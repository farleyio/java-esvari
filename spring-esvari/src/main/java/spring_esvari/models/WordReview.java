package spring_esvari.models;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Entity
@Table(name = "wordsreview")
@Getter
@Setter
@NoArgsConstructor // empty constructor for JPA
public class WordReview {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;

    @Column(nullable = false)
    private String wordEnglish;

    @Column(nullable = false)
    private String wordSpanish;

    @Column(length = 1024)
    private String description;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private Country country;

    @Column(nullable = false)
    private String resultAfterReview;

    public WordReview(String wordEnglish,
            String wordSpanish,
            Country country,
            String description,
            String resultAfterReview) {
        this.wordEnglish = wordEnglish;
        this.wordSpanish = wordSpanish;
        this.country = country;
        this.description = description;
        this.resultAfterReview = resultAfterReview;
    }
}
