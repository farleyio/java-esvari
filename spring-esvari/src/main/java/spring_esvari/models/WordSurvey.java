package spring_esvari.models;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Entity
@Table(name = "wordssurvey")
@Getter
@Setter
@NoArgsConstructor // empty constructor for JPA
public class WordSurvey {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;

    @Column(nullable = false)
    private String wordEnglish;

    @Column(nullable = false)
    private String wordSpanish;

    @Column(length = 1024)
    private String description;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private Country country;

    @Column
    private int amountOfTimesSelectedInSurvey;

    public WordSurvey(String wordEnglish,
            String wordSpanish,
            Country country,
            String description,
            int amountOfTimesSelectedInSurvey) {
        this.wordEnglish = wordEnglish;
        this.wordSpanish = wordSpanish;
        this.country = country;
        this.description = description;
        this.amountOfTimesSelectedInSurvey = amountOfTimesSelectedInSurvey;
    }
}
