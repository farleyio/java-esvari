package spring_esvari.bll;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spring_esvari.models.WordReview;
import spring_esvari.models.WordSurvey;
import spring_esvari.services.WordReviewRepository;
import spring_esvari.services.WordReviewServiceable;
import spring_esvari.services.WordSurveyRepository;
import spring_esvari.services.WordSurveyServiceable;

import java.util.List;
@Service
public class WordSurveyService implements WordSurveyServiceable {
    private final WordSurveyRepository wordSurveyRepository;
    @Autowired
    public WordSurveyService(WordSurveyRepository wordSurveyRepository) {
        this.wordSurveyRepository = wordSurveyRepository;
    }

    @Override
    public List<WordSurvey> getAllWordReview() {
        return wordSurveyRepository.findAll();
    }

    @Override
    public List<WordSurvey> searchWords(String search) {
        return wordSurveyRepository.findByWordEnglishContainingIgnoreCaseOrWordSpanishContainingIgnoreCase(search, search);
    }
}