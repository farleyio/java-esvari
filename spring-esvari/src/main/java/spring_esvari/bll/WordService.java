package spring_esvari.bll;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spring_esvari.models.Word;
import spring_esvari.services.WordRepository;
import spring_esvari.services.WordServiceable;

import java.util.ArrayList;
import java.util.UUID;
@Service
public class WordService implements WordServiceable {
    private final WordRepository wordRepository;
    @Autowired
    public WordService(WordRepository wordRepository) {
        this.wordRepository = wordRepository;
    }
    @Override
    public Word wordById(UUID id) {
        return wordRepository.findById(id).orElse(null);
    }
    @Override
    public void saveWord(Word word) {
        wordRepository.save(word);
    }

    @Override
    public void updateWord(Word word) {
        wordRepository.save(word);
    }

    @Override
    public ArrayList<Word> searchWords(String search) {
        return wordRepository.findByWordEnglishContainingIgnoreCaseOrWordSpanishContainingIgnoreCase(search, search);
    }

}
