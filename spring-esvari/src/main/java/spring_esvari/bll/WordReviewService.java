package spring_esvari.bll;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spring_esvari.models.WordReview;
import spring_esvari.services.WordReviewRepository;
import spring_esvari.services.WordReviewServiceable;

import java.util.List;
@Service
public class WordReviewService implements WordReviewServiceable {
    private final WordReviewRepository wordReviewRepository;
    @Autowired
    public WordReviewService(WordReviewRepository wordReviewRepository) {
        this.wordReviewRepository = wordReviewRepository;
    }

    @Override
    public List<WordReview> getAllWordReview() {
        return wordReviewRepository.findAll();
    }

    @Override
    public List<WordReview> searchWords(String search) {
        return wordReviewRepository.findByWordEnglishContainingIgnoreCaseOrWordSpanishContainingIgnoreCase(search, search);
    }
}