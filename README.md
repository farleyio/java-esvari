# java-esvari

install dependecies
```
pnpm install
```

running the project
```
mvn spring-boot:run
pnpm run dev
```

```
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
ALTER TABLE words ALTER COLUMN id SET DEFAULT uuid_generate_v4();
\d words
```

```
podman exec -i CONTAINERNAME -U USERNAME -d DBNAME < ~/insert_words.sql
```

Podman config
```
/etc/containers/registries.conf
[registries.search]
registries = ['docker.io']
```

