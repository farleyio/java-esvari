import pandas as pd

file_path = 'file.csv'
data = pd.read_csv(file_path)

with open('insert_words.sql', 'w') as f:
    f.write("-- Insert data into your_table_name\n")
    f.write("INSERT INTO words (id, country, description, word_english, word_spanish, amount_of_times_selected_in_survey)\nVALUES\n")

    for index, row in data.iterrows():
        values = f"(uuid_generate_v4(), '{row['country']}', '{row['description']}', '{row['word_english']}', '{row['word_spanish']}', '{row['amount_of_times_selected_in_survey']})"
        if index < len(data) - 1:
            values += ",\n"
        else:
            values += ";\n"
        f.write(values)
